﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Triunghi2
{
    class Triunghi2 : Form
    {
        double a, b, c;
        double A, B, C;
        static Form form1;
        GraphicsPath path;
        public new static void Main()
        {
            Application.Run(new Triunghi2());
        }
        public Triunghi2()
        {
            Text = "TRIUNGHI 2";
            BackColor = SystemColors.Window;
            ForeColor = SystemColors.WindowText;
            ResizeRedraw = true;

            Panel panou = new Panel();
            panou.BorderStyle = BorderStyle.FixedSingle;
            panou.Parent = this;
            panou.Size = new Size(25 * Font.Height, 2 * Font.Height);

            Label label = new Label();
            label.Location = new Point(0, 0);
            label.Text = "laturile:";
            label.Parent = panou;
            label.AutoSize = true;

            TextBox txtboxa = new TextBox();
            txtboxa.Parent = panou;
            txtboxa.Location = new Point(5 * Font.Height, 0 * Font.Height);
            txtboxa.Size = new Size(3 * Font.Height, Font.Height);
            txtboxa.TextChanged += new EventHandler(txtboxa_TextChanged);

            TextBox txtboxb = new TextBox();
            txtboxb.Parent = panou;
            txtboxb.Location = new Point(9 * Font.Height, 0 * Font.Height);
            txtboxb.Size = new Size(3 * Font.Height, Font.Height);
            txtboxb.TextChanged += new EventHandler(txtboxb_TextChanged);

            TextBox txtboxc = new TextBox();
            txtboxc.Parent = panou;
            txtboxc.Location = new Point(13 * Font.Height, 0 * Font.Height);
            txtboxc.Size = new Size(3 * Font.Height, Font.Height);
            txtboxc.TextChanged += new EventHandler(txtboxc_TextChanged);

            Button b1 = new Button();
            b1.Parent = panou;
            b1.Text = "gata";
            b1.Location = new Point(17 * Font.Height, 0 * Font.Height);
            b1.Size = new Size(3 * Font.Height, 2* Font.Height);
            b1.Click += new EventHandler(b1_Click);

            path = new GraphicsPath();
        }

        void txtboxa_TextChanged(object obj, EventArgs e)
        {
            TextBox txtbox = (TextBox)obj;
            a = Convert.ToInt32(txtbox.Text);
        }
        void txtboxb_TextChanged(object obj, EventArgs e)
        {
            TextBox txtbox = (TextBox)obj;
            b = Convert.ToInt32(txtbox.Text);
        }
        void txtboxc_TextChanged(object obj, EventArgs e)
        {
            TextBox txtbox = (TextBox)obj;
            c = Convert.ToInt32(txtbox.Text);
        }
        void b1_Click(object obj, EventArgs ea)
        {
                C = Math.Acos((a * a + b * b - c * c) / (2 * a * b));
                path.AddLine(0, 0, (float)a, 0);
                path.AddLine((float)a, 0, (float)a - (float)b * (float)Math.Cos(C), (float)b * (float)Math.Sin(C));
                path.CloseFigure();
                C = C * 180 / Math.PI;
                Invalidate();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            SizeF sf = g.VisibleClipBounds.Size;//da dim. zonei client in unit conforme cu PageUnit si PageScale - aici pixel
            g.PageUnit = GraphicsUnit.Millimeter;
            RectangleF rectf = new RectangleF();

            if ((a + b) > c && (a + c) > b && (b + c) > a && a >= 0 && b >= 0 && c >= 0 )
            {
            rectf = path.GetBounds();
            rectf.Width *= 1.08f;
            rectf.Height *= 1.08f;
            g.PageScale = Math.Min(sf.Width * 25.4f / (rectf.Width * g.DpiX+0.01f), sf.Height * 25.4f / (rectf.Height * g.DpiY+0.01f));
            g.TranslateTransform(rectf.Width / 20, rectf.Height / 20);
            g.FillRectangle(Brushes.Magenta, rectf);

                g.DrawPath(new Pen(ForeColor), path);
                Brush brush = new SolidBrush(ForeColor);
                PointF pt1 = new PointF(rectf.Width / 2, rectf.Height / 10 + 1);
                g.DrawString(a.ToString(), Font, brush, pt1);
                PointF pt2 = new PointF(rectf.Width / 4, rectf.Height / 4);
                g.TranslateTransform(rectf.Width, 0);
                g.RotateTransform(180f - (float)C);

                g.DrawString(C.ToString(), Font, brush, pt2);
            }
            else
            {
                String str = "" + a + ", " + b + ", " + c + "nu pot fi laturile unui triunghi.";
                Brush brush = new SolidBrush(ForeColor);
                PointF pt = new PointF(1, sf.Height/8+1);
                g.DrawString(str, Font, brush, pt);
            }
        }
    }
}

